## Clonar repositório
```
git clone https://gitlab.com/moreiraandre/flex-trab2.git
```

## Instalar _flex_ e _bison_
```
cd flex-trab2
chmod +x install.sh
./install.sh
```

## Compilar
```
make
```

## Teste
```
./algoritmo codigo
```