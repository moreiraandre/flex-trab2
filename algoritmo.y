%{
#include <cstdio>
#include <cstdlib>
#include <string>
#include <map>

using namespace std;

map<std::string, double> vars;
extern int yylex();
extern void yyerror(char *);
void DivError(void);
void UnknownVarError(std::string s);
%}

%union {
    int     int_val;
    double  double_val;
    std::string* str_val;
}

%token <int_val> ponto virgula ponto_virgula dois_pontos abre_col fecha_col abre_par fecha_par aspas identificador num_inteiro num_real const_lit op_arit_mult op_arit_div op_arit_adi op_ari_sub op_atrib op_rel_igual op_rel_naoigual op_rel_maior op_rel_maiorigual op_rel_menor op_rel_menorigual op_log_nao op_log_and op_log_or pr_algoritmo pr_inicio pr_fim_algo pr_logico pr_inteiro pr_real pr_caracter pr_registro pr_leia pr_escreva pr_se pr_entao pr_senao pr_fim_se pr_para pr_ate pr_passo pr_faca pr_fim_para pr_enqto pr_fim_enqto pr_repita pr_abs pr_trunca pr_resto pr_declare abra_par cons_lit fechar_par op_arit_adicao op_arit_expo op_arit_rad op_logico_and op_logico_or op_rel_maiorque op_rel_menorque pr_entrada pr_fim_funcao pr_fim_procmto pr_funcao pr_incio pr_procmto pr_saida op_arit_subtracao

%start ini_alg

%%
ini_alg:           ALGO | pr_fim_algo;
ALGO:              pr_algoritmo identificador PROCS pr_incio DECL CMDS pr_fim_algo;
DECL:              pr_declare L_IDS dois_pontos TIPO ponto_virgula DECL;
L_IDS:             identificador COMP LIDS;
LIDS:              virgula L_IDS;
COMP:              abre_col DIM fecha_col;
DIM:               num_inteiro ponto ponto num_inteiro DIMS;
DIMS:              virgula DIM;
TIPO:              pr_logico
                   | pr_caracter
                   | pr_inteiro
                   | pr_real
                   | identificador
                   | REG;
REG:               pr_registro abra_par DECL fecha_par;
CMDS:              pr_leia L_VAR
                   | pr_escreva L_ESC
                   | identificador op_atrib EXP
                   | pr_se COND pr_entao CMDS SEN pr_fim_se
                   | pr_para identificador op_atrib num_inteiro pr_ate num_inteiro pr_passo num_inteiro pr_faca CMDS pr_fim_para
                   | pr_enqto COND CMDS pr_fim_enqto
                   | pr_repita CMDS pr_ate COND
                   | identificador abre_par L_VAR fecha_par;
L_VAR:             VAR L_VRS;
L_VRS:             virgula VAR;
VAR:               identificador IND;
IND:               abre_col num_inteiro fecha_col IND
                   | ponto identificador IND;
L_ESC:             cons_lit L_ESCS
                   | VAR L_ESCS;
L_ESCS:            virgula L_ESC;
SEN:               pr_senao CMDS;
PROCS:             pr_funcao identificador pr_entrada L_VAR pr_saida L_VAR DECL CMDS pr_fim_funcao
                   | pr_procmto identificador pr_entrada L_VAR DECL CMDS pr_fim_procmto;
EXP:               EXP_L
                   | EXP_A;
EXP_A:             TERM_A MULDIV EXP_A
                   | TERM_A;
TERM_A:            FAT_A ADISUB TERM_A
                   | FAT_A;
FAT_A:             EXP_A op_arit_expo EXP_A
                   | EXP_A op_arit_rad EXP_A
                   | abre_par EXP_A fecha_par
                   | FUNC abre_par L_VAR fecha_par
                   | VAR
                   | num_inteiro
                   | num_real;
MULDIV:            op_arit_mult
                   | op_arit_div;
ADISUB:            op_arit_adicao
                   | op_arit_subtracao;
FUNC:              pr_abs
                   | pr_trunca
                   | pr_resto;
EXP_L:             REL OP_LOG EXP_L
                   | op_log_nao abre_par REL fechar_par
                   | REL;
REL:               FAT_R OP_REL FAT_R;
FAT_R:             FAT_A
                   | const_lit;
OP_LOG:            op_logico_and
                   | op_logico_or;
OP_REL:            op_rel_igual
                   | op_rel_naoigual
                   | op_rel_maiorque
                   | op_rel_maiorigual
                   | op_rel_menorque
                   | op_rel_menorigual;
COND:              abre_par EXP_L fecha_par;
%%

void DivError(void) {
    printf("DIVISÃO POR ZERO!\n");
    exit(0);
}

void UnknownVarError(std::string s) {
    printf("VARIÁVEL NÃO ESCRITA! = %s\n", s.c_str());
    exit(0);
}
