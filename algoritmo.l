%{
#include <cstdio>
#include <cstdlib>
#include <string>
#include "algoritmo.tab.h"

using namespace std;

void yyparser(void);
void yyerror(char*);
%}

digito                                   [0-9]
letra                                    [a-zA-Z]

%%
"."                                      return ponto;
","                                      return virgula;
";"                                      return ponto_virgula;
":"                                      return dois_pontos;
"["                                      return abre_col;
"]"                                      return fecha_col;
"("                                      return abre_par;
")"                                      return fecha_par;
"\""                                     return aspas;
{digito}+                                return identificador;
{digito}+","{digito}+                    return num_inteiro;
{letra}                                  return num_real;
{letra}({letra}|{digito})31              return const_lit;
"*"                                      return op_arit_mult;
"/"                                      return op_arit_div;
"+"                                      return op_arit_adi;
"-"                                      return op_ari_sub;
"="                                      return op_atrib;
"=="                                     return op_rel_igual;
"!="                                     return op_rel_naoigual;
">"                                      return op_rel_maior;
">="                                     return op_rel_maiorigual;
"<"                                      return op_rel_menor;
"<="                                     return op_rel_menorigual;
"!"                                      return op_log_nao;
"&&"                                     return op_log_and;
"||"                                     return op_log_or;

"algoritmo"                              return pr_algoritmo;
"Inicio"                                 return pr_inicio;
"fim_algoritmo"                          return pr_fim_algo;
"LOGICO"                                 return pr_logico;
"INTEIRO"                                return pr_inteiro;
"REAL"                                   return pr_real;
"CARACTER"                               return pr_caracter;
"REGISTRO"                               return pr_registro;
"leia"                                   return pr_leia;
"escreva"                                return pr_escreva;
"se"                                     return pr_se;
"entao"                                  return pr_entao;
"senao"                                  return pr_senao;
"fim_se"                                 return pr_fim_se;
"para"                                   return pr_para;
"ate"                                    return pr_ate;
"passo"                                  return pr_passo;
"faça"                                   return pr_faca;
"fim_para"                               return pr_fim_para;
"enquanto"                               return pr_enqto;
"fim_enquanto"                           return pr_fim_enqto;
"repita"                                 return pr_repita;
"ABS"                                    return pr_abs;
"TRUNCA"                                 return pr_trunca;
"RESTO"                                  return pr_resto;
"declare"                                return pr_declare;
%%

void yyerror(char *str) {
    printf("ERRO NO PARSER!\n");
}

int yywrap(void) {}

int main (int num_args, char **args) {
    if (num_args != 2) exit(0);
    FILE *file = fopen(args[1], "r");
    if (file == NULL) exit(0);
    yyin = file;
    yyparse();
    fclose(file);
}
