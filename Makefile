all:
	bison -d -Wnone algoritmo.y
	flex algoritmo.l
	g++ -Wno-write-strings -o algoritmo lex.yy.c algoritmo.tab.c
	
clean:
	rm -f algoritmo lex.yy.c *.tab.*

